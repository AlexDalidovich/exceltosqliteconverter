﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Data.OleDb;

namespace CSharpImport
{
    public partial class Form1 : Form
    {
        
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Excel Tables| *.xls";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            path = openFileDialog1.FileName;
            textBox1.Text = path;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                DataTable table = UploadExcelFile(".xls", "1", textBox1.Text);
                if (table == null)
                    MessageBox.Show("Incorrect path");
                else
                    SQLiteSaving(table);
            }
            else MessageBox.Show("Select the path!!!");
        }

        public void SQLiteSaving(DataTable table)
        {
            DataTable tbls = con.GetSchema("Tables", new string[] { null, null, null, "TABLE" });
            label1.Text = tbls.Rows[0]["TABLE_NAME"].ToString();
            SQLiteConnection connection;
            SQLiteCommand command;

        }

        public static DataTable UploadExcelFile(string strFileType, string sheetName, string strNewPath)
        {
            #region class objects
            con = new OleDbConnection(""); ;
            cmd = new OleDbCommand();
            da = new OleDbDataAdapter();
            string query = null;
            string connString = string.Empty;
            #endregion
            //Connection String to Excel
            if (strFileType.Trim() == ".xls")
                connString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + strNewPath + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
            else if (strFileType.Trim() == ".xlsx")
                connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + strNewPath + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
            
            query = "SELECT * FROM [" + sheetName + "$]";
            try
            {
                con = new OleDbConnection(connString);
                if (con.State == ConnectionState.Closed) con.Open();
                cmd = new OleDbCommand(query, con);
                da = new OleDbDataAdapter(cmd);
                ds = new DataSet();
                da.Fill(ds, sheetName);
            }
            catch (Exception ex)
            {
                MessageBox.Show("error");
                return null;
            }
            finally
            {
         //       da.Dispose();
           //     con.Close();
             //   con.Dispose();
            }
            return ds.Tables[0];
        }
    }
}
